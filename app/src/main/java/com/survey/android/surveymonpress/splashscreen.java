package com.survey.android.surveymonpress;

import android.app.Dialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;



public class splashscreen extends AppCompatActivity {
EditText mEditLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        mEditLocation = findViewById(R.id.location);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mEditLocation.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                            loadingProgress();
                            startActivity(new Intent(splashscreen.this,MainActivity.class).
                            putExtra("location",String.valueOf(mEditLocation.getText())));

                    return true;
                }
                return false;
            }
        });
    }

    public void loadingProgress() {

        final Dialog dialog = new Dialog(splashscreen.this);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();


        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {

                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                dialog.dismiss();

            }

        }.start();

    }
}
