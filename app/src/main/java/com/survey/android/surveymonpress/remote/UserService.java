package com.survey.android.surveymonpress.remote;

import com.survey.android.surveymonpress.model.RatingResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface UserService {

    @FormUrlEncoded
    @POST("insert")
    Call<RatingResponse> insert(@Field("location") String location,
                               @Field("rating") Integer password);


}
