package com.survey.android.surveymonpress;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.survey.android.surveymonpress.model.RatingResponse;
import com.survey.android.surveymonpress.remote.UserService;
import com.survey.android.surveymonpress.remote.UtilsApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView timer;
    TextView mTextTagLocation;
    String location;
    UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextTagLocation = findViewById(R.id.tagLocation);
        mTextTagLocation.setText(getIntent().getExtras().getString("location"));
        this.location = getIntent().getExtras().getString("location");
        userService = UtilsApi.getUserServices();

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void feedback(View view) {

        //feedbackPopUp();
        loadingProgress();

    }

    public void loadingProgress() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.loading);
        dialog.setCancelable(false);
        dialog.show();


        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {

                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                dialog.dismiss();
                feedbackPopUp();

            }

        }.start();

    }

    public void feedbackPopUp() {


        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final PopupWindow pw = new PopupWindow(inflater.inflate(R.layout.terimakasih_dialog, null, false),
                400,400, true);


        View myPoppyView = pw.getContentView();
        myPoppyView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.expand_in));
        pw.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        pw.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        pw.showAtLocation(this.findViewById(R.id.mainActivityLayout), Gravity.TOP, 0, 0);


        timer = myPoppyView.findViewById(R.id.timer);
        timer.setText(String.valueOf("kosong"));

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("akan menutup dalam " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                pw.dismiss();

            }

        }.start();
    }


    public void insert(int rating){
        Call<RatingResponse> call = userService.insert(this.location,rating);
        call.enqueue(new Callback<RatingResponse>() {
            @Override
            public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                loadingProgress();
            }

            @Override
            public void onFailure(Call<RatingResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this,"GAGAL"+t,Toast.LENGTH_SHORT).show();

            }
        });
    }
    public void feedback1(View view) {

        Integer rating = 1;
        insert(rating);

    }

    public void feedback2(View view) {

        Integer rating = 2;
        insert(rating);

    }
    public void feedback3(View view) {

        Integer rating = 3;
        insert(rating);

    }
    public void feedback4(View view) {

        Integer rating = 4;
        insert(rating);

    }
    public void feedback5(View view) {

        Integer rating = 5;
        insert(rating);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Tidak Bisa Keluar dari Aplikasi", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_MAIN.equals(intent.getAction())) {
            Toast.makeText(this, "Tidak Bisa Keluar dari Aplikasi", Toast.LENGTH_LONG).show();

        }
    }

}
